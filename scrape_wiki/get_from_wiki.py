import json

from bs4 import BeautifulSoup
import requests

def parseValue(value):
    """
        Parse value
        Return integer (O if null)
    """
    # Strip
    res = value.strip()
    if res == '':
        return 0
    else:
        return int(res)

def getInfos(soupObj, selector, spells_page=False):
    """
        Parse and extract data
        Return list of dictionnary
    """
    items = soupObj.find('div',attrs={"class":u"table %s" %selector}).find("table").find_all("tr")
    res = []
    for item in items:
        its = item.find_all("td")

        if len(its) > 1:
            if spells_page == False:
                res.append({
                    "name" : its[0].text,
                    "level" : 0,
                    "strength" : parseValue(its[1].text),
                    "endurance" : parseValue(its[2].text),
                    "agility" : parseValue(its[3].text),
                    "intelligence" : parseValue(its[4].text),
                    "wisdom" : parseValue(its[5].text)
                })
            if spells_page == True:
                res.append({
                    "name" : its[0].text,
                    "level" : parseValue(its[1].text),
                    "strength" : parseValue(its[2].text),
                    "endurance" : 0,
                    "agility" : 0,
                    "intelligence" : parseValue(its[3].text),
                    "wisdom" : parseValue(its[4].text)
                })
    return res

res = {}


# Armors

wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/armurerie")
soup = BeautifulSoup(wiki_page.text, "html.parser")

#Grab infos
## Helmets
res["helmets"] = getInfos(soup, "sectionedit3")
## Chests
res["chests"] = getInfos(soup, "sectionedit5")
## Belts
res["belts"] = getInfos(soup, "sectionedit7")
## Legs
res["leggs"] = getInfos(soup, "sectionedit9")
## Boots
res["boots"] = getInfos(soup, "sectionedit11")
## Gloves
res["gloves"] = getInfos(soup, "sectionedit13")
## Shields
res["shields"] = getInfos(soup, "sectionedit15")

## Robes
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/robes")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["robes"] = getInfos(soup, "sectionedit3")
res["robes"].extend(getInfos(soup, "sectionedit5"))
res["robes"].extend(getInfos(soup, "sectionedit7"))
res["robes"].extend(getInfos(soup, "sectionedit9"))
res["robes"].extend(getInfos(soup, "sectionedit11"))
res["robes"].extend(getInfos(soup, "sectionedit13"))
res["robes"].extend(getInfos(soup, "sectionedit16"))
res["robes"].extend(getInfos(soup, "sectionedit18"))
res["robes"].extend(getInfos(soup, "sectionedit20"))

## Merge chests and robes see issues https://gitlab.com/t4c_stuff/t4c_persorama/-/issues/8
res["chests"].extend(res["robes"])
del res["robes"]


## Cloack
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/capes")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["cloacks"] = getInfos(soup, "sectionedit3")
res["cloacks"].extend(getInfos(soup, "sectionedit6"))
res["cloacks"].extend(getInfos(soup, "sectionedit8"))
res["cloacks"].extend(getInfos(soup, "sectionedit10"))
res["cloacks"].extend(getInfos(soup, "sectionedit12"))
res["cloacks"].extend(getInfos(soup, "sectionedit14"))

## Amulets
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/amulettes")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["amulets"] = getInfos(soup, "sectionedit3")
res["amulets"].extend(getInfos(soup, "sectionedit5"))
res["amulets"].extend(getInfos(soup, "sectionedit7"))
res["amulets"].extend(getInfos(soup, "sectionedit9"))
res["amulets"].extend(getInfos(soup, "sectionedit11"))

## Rings
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/anneaux")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["rings"] = getInfos(soup, "sectionedit3")
res["rings"].extend(getInfos(soup, "sectionedit5"))
res["rings"].extend(getInfos(soup, "sectionedit7"))
res["rings"].extend(getInfos(soup, "sectionedit9"))
res["rings"].extend(getInfos(soup, "sectionedit11"))

## Bracelets
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/bracelets")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["bracelets"] = getInfos(soup, "sectionedit3")
res["bracelets"].extend(getInfos(soup, "sectionedit5"))
res["bracelets"].extend(getInfos(soup, "sectionedit7"))
res["bracelets"].extend(getInfos(soup, "sectionedit9"))
res["bracelets"].extend(getInfos(soup, "sectionedit11"))
res["bracelets"].extend(getInfos(soup, "sectionedit13"))

## Orbs
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/orbes")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["orbs"] = getInfos(soup, "sectionedit3")
res["orbs"].extend(getInfos(soup, "sectionedit5"))
res["orbs"].extend(getInfos(soup, "sectionedit7"))
res["orbs"].extend(getInfos(soup, "sectionedit9"))
res["orbs"].extend(getInfos(soup, "sectionedit11"))

# Weapons

wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/arcsetfleches")
soup = BeautifulSoup(wiki_page.text, "html.parser")

#Grab infos
## Bows
res["bows"] = getInfos(soup, "sectionedit3")
res["bows"].extend(getInfos(soup, "sectionedit7"))
res["bows"].extend(getInfos(soup, "sectionedit11"))
res["bows"].extend(getInfos(soup, "sectionedit15"))

## Quivers
res["quivers"] = getInfos(soup, "sectionedit5")
res["quivers"].extend(getInfos(soup, "sectionedit9"))
res["quivers"].extend(getInfos(soup, "sectionedit13"))
res["quivers"].extend(getInfos(soup, "sectionedit17"))

## Swords
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/objets/armesmelee")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["swords"] = getInfos(soup, "sectionedit3")
res["swords"].extend(getInfos(soup, "sectionedit5"))
res["swords"].extend(getInfos(soup, "sectionedit7"))
res["swords"].extend(getInfos(soup, "sectionedit9"))

## Merge swords and bows
res["swords"].extend(res["bows"])
del res["bows"]

## Merge shields and quivers
res["shields"].extend(res["quivers"])
del res["quivers"]

## Spells
wiki_page = requests.get("https://www.t4c-neerya.com/wiki/sorts_et_competences/accueil")
soup = BeautifulSoup(wiki_page.text, "html.parser")

res["spells"] = getInfos(soup, "sectionedit2", spells_page=True)

# Sort items see issues https://gitlab.com/t4c_stuff/t4c_persorama/-/issues/7
for (key, values) in res.items():
    res[key] = sorted(values, key=lambda k: k["name"])
    
# Write
with open("../assets/items.json", "w") as outfile:
    json.dump(res, outfile)

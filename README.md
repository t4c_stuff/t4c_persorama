# T4C Persorama

Persorama pour T4C

Le persorama est disponible à l'adresse suivante [https://t4c_stuff.gitlab.io/t4c_persorama](https://t4c_stuff.gitlab.io/t4c_persorama)

Ce persorama est pour le moment exclusif au serveur [T4C Neerya](https://www.t4c-neerya.com/) (contient les addons du serveur).

## Données

Les données sont extraites via le [Wiki](https://www.t4c-neerya.com/wiki/) mis à disposition par les joueurs.

Les données sont disponibles au [format JSON](https://gitlab.com/t4c_stuff/t4c_persorama/-/blob/master/assets/items.json)
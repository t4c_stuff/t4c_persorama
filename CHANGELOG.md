# [1.7.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.6.0...v1.7.0) (2021-01-02)


### Features

* **front:** Modification de l'interface, ajout de deux lignes sorts, ajout de 'collapse' ([20c6bcd](https://gitlab.com/t4c_stuff/t4c_persorama/commit/20c6bcd50fb915532d5ea6b86c8f9574476389d6))

# [1.6.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.5.1...v1.6.0) (2021-01-01)


### Bug Fixes

* Ajout de la partie Sorts ([e8eb9b4](https://gitlab.com/t4c_stuff/t4c_persorama/commit/e8eb9b4d68da733f08e4af7e51f70e377b9fbc3e))


### Features

* **scrape:** Récupération de la partie sorts ([8cd13d8](https://gitlab.com/t4c_stuff/t4c_persorama/commit/8cd13d8e2e6b957e751dd6067c63295f2698799c))

## [1.5.1](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.5.0...v1.5.1) (2020-12-30)


### Bug Fixes

* **docs:** Modification du README.md, ajout de LICENSE et des sources ([1ca94b4](https://gitlab.com/t4c_stuff/t4c_persorama/commit/1ca94b46358c1096084068add2d54a409a9fbfda))

# [1.5.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.4.0...v1.5.0) (2020-12-30)


### Features

* **front:** Ajout d'un tri par ordre alphabétique ([059aee3](https://gitlab.com/t4c_stuff/t4c_persorama/commit/059aee3394dcb8944b150935440b49cfa6432767))

# [1.4.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.3.0...v1.4.0) (2020-12-30)


### Features

* **front:** Regroupement plastrons et robes ([3beaac9](https://gitlab.com/t4c_stuff/t4c_persorama/commit/3beaac98c54d1694b113f6843b03d5960b8231cf))

# [1.3.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.2.0...v1.3.0) (2020-12-27)


### Features

* **front:** Changement du framework ([f1a968b](https://gitlab.com/t4c_stuff/t4c_persorama/commit/f1a968b37e40741c0009c313863f2fc4400adc12))

# [1.2.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.1.0...v1.2.0) (2020-12-08)


### Features

* **items:** Ajout des nouveaux objets ([6a8af45](https://gitlab.com/t4c_stuff/t4c_persorama/commit/6a8af458e7a02c9b1edfc39d0ff3739ef08c5cb5))

# [1.1.0](https://gitlab.com/t4c_stuff/t4c_persorama/compare/v1.0.0...v1.1.0) (2020-12-08)


### Features

* **ui:** Ajout CSS ([6951530](https://gitlab.com/t4c_stuff/t4c_persorama/commit/6951530f2d5de99f45f039c620dc169c5b42df92))

# 1.0.0 (2020-12-07)


### Bug Fixes

* **ci:** Correction du CI ([ffdbc62](https://gitlab.com/t4c_stuff/t4c_persorama/commit/ffdbc62065f63e8bf0bddcdcd21761561b0ee92a))
* **ci:** Correction du CI ([fe595b5](https://gitlab.com/t4c_stuff/t4c_persorama/commit/fe595b5ea8e5e288f6e6daa9f1848dd08e1e7984))
* **ci:** Correction du CI ([9ba8a29](https://gitlab.com/t4c_stuff/t4c_persorama/commit/9ba8a295b7da03f1429669c15fa94bebc5c8c314))
* **ci:** Correction release ([8f75d1f](https://gitlab.com/t4c_stuff/t4c_persorama/commit/8f75d1fea90ffc84a2ca4c090c34ef6c7a6673cc))


### Features

* **ci:** Ajout semantic release ([e45a238](https://gitlab.com/t4c_stuff/t4c_persorama/commit/e45a238dab53025d38541322d27b6b7f8ed98e2f))

const stats = ["strength", "endurance", "agility", "wisdom", "intelligence"]
const typeArmors = ["helmets", "chests", "belts", "gloves", "leggs", "boots", "shields", "amulets", "rings", "bracelets", "orbs", "cloacks", "swords", "spells", "spells2"]
var default_stat = 20

function getRessource(ressource){
    /*
    Load ressource file
    Return Object
    */
    var response = null;
    $.ajax({
        url: ressource,
        type: "GET",
        datatype: "json",
        async: false,
        success: function(data) {
            response = data
        }
    });
    response["spells2"] = response["spells"]
    return response;
}

function resetToDefaultValue() {
    /*
    Reset to default value
    */
    stats.forEach(stat => $("#" + stat).val(default_stat));
}

function populateData(data, typeData){
    /*
    Populate select with data
    */
    if (typeData == "armors") {
        Object.keys(data).forEach(armor => 
            data[armor].forEach(item => 
                $("#" + armor).append($("<option />")
                    .val(item.short_name).
                    text(item.name).
                    attr({
                        "data-strength" : parseInt(item.strength),
                        "data-endurance" : parseInt(item.endurance),
                        "data-agility" : parseInt(item.agility),
                        "data-wisdom" : parseInt(item.wisdom),
                        "data-intelligence" : parseInt(item.intelligence)
                    })),
                $("#" + armor).change(function() {
                    setAndCalculate();
                })
            )
        )
    }
}

function calculateLevel(){
    /*
    Return max level for stat
    */
    var sumStats = 0;
    stats.forEach(stat => sumStats += (parseInt($("#" + stat).val() - default_stat)));
    return Math.round(sumStats / 5) +1;
}

function setAndCalculate(){
    /*
    Set value from item and calculate stat
    We use all stat to get max on each
    */
    // Reset to default
    resetToDefaultValue();
    // We need to re apply for each armor
    typeArmors.forEach(typeArmor => stats.forEach(stat => $("#" + stat).val(Math.max($("#" + typeArmor + " option:selected").data(stat), $("#" + stat).val()))))

    // Set max level
    $("#level").val(calculateLevel());
   
}